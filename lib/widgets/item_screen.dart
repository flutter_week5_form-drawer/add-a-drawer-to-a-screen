import 'package:flutter/material.dart';

class ItemScreen2 extends StatelessWidget {
  const ItemScreen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('item 2!'),
    );
  }
}

class ItemScreen3 extends StatelessWidget {
  const ItemScreen3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('item 3!'),
    );
  }
}